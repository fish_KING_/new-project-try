const{ObjectID} = require('mongodb');


let stored;
exports.initCollection = (db) =>{
    stored = db.collection('data');
}
exports.find = () => stored.find().toArray();

exports.findById = id => stored.findOne({_id: ObjectID(id)});

exports.save = (story) =>  stored.insertOne(story);

//exports.updateById = (id, newStory) => stored.findOneAndUpdate({_id: ObjectID(id)}, {$set: {title: newStory.title, content: newStory.content}});
    
exports.deleteById = id => stored.deleteOne({_id: ObjectID(id)});
//==================================================================================================================================================