const model = require('../models/story');
exports.index = (req, res, next)=>{
    model.find()
    .then(stories=>res.render('./story/index', {stories}))
    .catch(err=>next(err));
    
};

exports.new = (req, res)=>{
    res.render('./story/new');
};

exports.create = (req, res, next)=>{
    let story = req.body;
    story.createdAt = new Date();
    story.display = ytURLtoYTembedURL_video(story.display);
    model.save(story)
    .then(result=>res.redirect('/data'))
    .catch(err=>next(err));
    
};

exports.show = (req, res, next)=>{
    let id = req.params.id;
    model.findById(id)
    .then(story=>{
        if(story) {
            res.render('./story/show', {story});
        } else {
            let err = new Error('Cannot find a story with id ' + id);
            err.status = 404;
            next(err);
        }
    })
    .catch(err => next(err));
    
   
};

exports.edit = (req, res, next)=>{
    let id = req.params.id;
    model.findById(id)
    .then(story=>{
        if(story) {
            res.render('./story/edit', {story});
        } else {
            let err = new Error('Cannot find a story with id ' + id);
            err.status = 404;
            next(err);
        }
    })
    .catch(err => next(err));
    
};

exports.update = (req, res, next)=>{
    let story = req.body;
    let id = req.params.id;
    model.updateById(id, story)
    .then(result=>{
        if(result.lastErrorObject.updatedExisting){
        res.redirect('/data/'+id);
        }else{
            let err = new Error('Cannot find a connection with id ' + id);
            err.status = 404;
            next(err);
        }
    })
    .catch(err => next(err));
};

exports.delete = (req, res, next)=>{
    let id = req.params.id;
    model.deleteById(id)
    .then(result=>{
        if(result){
            res.redirect('/data')
        }else{
            let err = new Error('Unable to delete a connection with id ' + id);
            err.status = 404;
            next(err);
        }
    })
    .catch(err => next(err));
};

function ytURLtoYTembedURL_video(link){
    let str2 = "";
    link = link.split("")
    for(let i=link.length; i>0; i--){
      if(link[i] !== '/'){
        if(link[i] !== undefined){
            str2 = str2+link[i];
          }
        }else{
            break;
        }
    }
    str2 = str2.split("").reverse();
    if(str2.includes("=")||str2.includes("&")){
      for(let i =0; i<str2.length; i++){
        if(str2[i] !== '='){
          str2[i] = undefined;
        }else{
          str2[i] = undefined;
          break;
        }
      }
     str2 = str2.reverse();
      for(let i =0; i<str2.length; i++){
        if(str2[i] !== '&'){
          str2[i] = undefined;
        }else{
          str2[i] = undefined;
          break;
        }
      }
      str2 = str2.reverse();
      let str3 = "";
      for(let i=0;i<str2.length; i++){
        if(str2[i]!==undefined){
          str3 = str3+str2[i];
        }
      }
      str2 = str3;
      str2 = "https://www.youtube.com/embed/"+str2;
    }else{
      str2 = str2.join("");
      str2 = "https://www.youtube.com/embed/"+str2;
    }
    return str2;
  }