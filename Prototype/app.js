//this is where the program starts to run
const express = require('express');
const morgan = require('morgan');
const methodOverride = require('method-override');
const {MongoClient} = require('mongodb');
const storyRoutes = require('./routes/storyRoutes');
const {initCollection} = require('./models/story');


const app = express();


let port = 443;
let host = 'localhost';
let url = 'mongodb://localhost:27017';
app.set('view engine', 'ejs');


app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));
app.use(morgan('tiny'));
app.use(methodOverride('_method'));

MongoClient.connect(url, { useUnifiedTopology: true })
.then((client => {
    db = client.db('storage');
    initCollection(db);
    app.listen(port, host, ()=>{
    console.log('Server is running on port', port);
        });
}))
.catch(err=>console.log(err.message));

app.get('/', (req, res)=>{
    res.render('index');
});

app.use('/data', storyRoutes);

app.use((req, res, next) => {
    let err = new Error('The server cannot locate ' + req.url);
    err.status = 404;
    next(err);

});

app.use((err, req, res, next)=>{
    console.log(err.stack);
    if(!err.status) {
        err.status = 500;
        err.message = ("Internal Server Error");
    }

    res.status(err.status);
    res.render('error', {error: err});
});